Bigish!
============

Just a really useless [shell](http://ratfactor.com/zig/forking-is-cool) written in zig.



## Setup And Usage

Clone this repo to your desktop and build with `zig build`

Run the executable and play around with it

---

## License

>You can check out the full license [here](https://git.ecma.fun/ecma/sikish/src/branch/master/LICENSE)

This project is licensed under the terms of the **WTFPL** license.
