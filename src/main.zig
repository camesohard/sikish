const std = @import("std");
const cmd = @import("commands.zig");
const helpers = @import("helpers.zig");
// Globals
const print = std.debug.print;
const colors = helpers.colors;

pub fn main() !void {
    const input = std.io.getStdIn().reader();
    try shellLoop(input);
}

fn shellLoop(input: std.fs.File.Reader) !void {
    var buffer: [1024]u8 = undefined;

    const max_args = 9;
    const max_args_size = 255;

    while (true) {
        // Get current directory
        var buf: [std.fs.MAX_PATH_BYTES]u8 = undefined;
        const cwd = try std.os.getcwd(&buf);

        // Prompt
        // Print current dir and =>
        print("{s}{s}{s} {s}=>{s} ", .{ colors.blue, cwd, colors.reset, colors.green, colors.reset });

        // Read input from user
        var input_str = (try input.readUntilDelimiterOrEof(buffer[0..], '\n')) orelse {
            // End of file. Print goodbye and exit
            print("Goodbye.\n", .{});
            return;
        };
        // User hit enter without any input; do nothing and continue
        if (input_str.len == 0) continue;

        // The command and arguments are null-terminated strings. These arrays are
        // storage for the strings and pointers to those strings.
        var args: [max_args][max_args_size:0]u8 = undefined;
        var args_ptrs: [max_args:null]?[*:0]u8 = undefined;

        // Split user input to "tokens"
        var tokens = std.mem.split(u8, input_str, " ");
        var i: u8 = 0;

        while (tokens.next()) |token| {
            std.mem.copy(u8, &args[i], token);
            args[i][token.len] = 0; // Add zero sentinel manually to the end of list
            args_ptrs[i] = &args[i];
            i += 1;
        }
        args_ptrs[i] = null; // Add null sentinel manually

        // Check if its a builtin function
        // And if it is; execute the function
        var its_a_builtin = try cmd.isBuiltin(max_args, args_ptrs);
        if (its_a_builtin) continue;

        // Create a new child process and execute the command
        try helpers.executeCmd(max_args, args_ptrs);
    }
}
