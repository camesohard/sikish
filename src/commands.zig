const std = @import("std");
const helpers = @import("helpers.zig");

const print = std.debug.print;
const colors = helpers.colors;

pub fn isBuiltin(comptime max_args: u8, args: [max_args:null]?[*:0]u8) !bool {
    var command: []const u8 = helpers.toStr(args[0].?);

    if (std.mem.eql(u8, command, "cd")) {
        if (args[1] == null) return true;
        var path = helpers.toStr(args[1].?);

        try changeDir(path);
        return true;
    } else if (std.mem.eql(u8, command, "exit")) {
        std.os.exit(0);
        // Don't return anything because we just end the program lol
    }

    return false; // Not a builtin
}

pub fn changeDir(path: []const u8) !void {
    if (path.len == 0) return; // No argument; don't do anything
    const errType = std.os.ChangeCurDirError;

    // Change current directory and catch common errors
    std.os.chdir(path) catch |err| {
        if (err == errType.FileNotFound) {
            helpers.handleError("cd: no such file or directory!");
        } else if (err == errType.NotDir) {
            helpers.handleError("cd: not a directory!");
        } else return err;
    };
}

// TODO: Add alias
