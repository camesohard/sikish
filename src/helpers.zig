// Helper functions and variables goes here
const std = @import("std");
const print = std.debug.print;

pub const Colors = struct {
    red: []const u8 = "\x1b[31m",
    blue: []const u8 = "\x1b[34m",
    green: []const u8 = "\x1b[32m",
    reset: []const u8 = "\x1b[0m",
};

pub const colors = Colors{};

pub fn toStr(arg: [*:0]u8) []const u8 {
    var str: []const u8 = undefined;

    // Find the last index of arg
    // Kinda like readUntilDelimiter
    var i: u8 = 0;
    while (true) {
        if (arg[i] == 0) break;
        i += 1;
    }
    // Copy all the chars to the new string
    str = arg[0..i];

    return str;
}

pub fn executeCmd(comptime max_args: u8, args: [max_args:null]?[*:0]u8) !void {
    const pid = try std.os.fork();

    if (pid == 0) {
        const errType = std.os.ExecveError;
        const env = [_:null]?[*:0]u8{"TERM=xterm-256color"};

        const err = std.os.execvpeZ(args[0].?, &args, &env);

        // This program shouldn't have gone this far
        // If we make it this far, then the command has failed and we have an error
        if (err == errType.FileNotFound) {
            handleError("sikish: command not found!");
        } else return err;

        std.os.exit(1); // Exit the child process with status 1
    } else {
        const wait_pid = std.os.waitpid(pid, 0);

        if (wait_pid.status != 0) print("{}\n", .{wait_pid.status});
    }
}

// TODO: Custom formatting
pub fn handleError(error_msg: []const u8) void {
    print("{s}{s}{s}\n", .{ colors.red, error_msg, colors.reset });
}
